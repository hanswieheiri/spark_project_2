# -*- coding: utf-8 -*-
"""
Created on 12 Jun 12.06.17 13:04 2017 

@author: trafrro

Description:

"""

import json
import socket
import time
import traceback
import os
import sys
from urllib.error import HTTPError

from pyspark import SparkContext
from pyspark.sql import SQLContext

from sbb import feed

sc = SparkContext("local", "SBB")
sc.setLogLevel("WARN")
print("Spark version", sc.version)

filebase = "real-time-landing"

spark = SQLContext(sc)


def input_normalizer(timestamp, data):
    """ normalize the json input into row format"""
    lines = []
    trip_id = data['trip_update']['trip']['trip_id']
    route_id = data['trip_update']['trip']['route_id']

    for s in data['trip_update']['stop_time_update']:
        stop_id = s['stop_id']
        if 'arrival' in s:
            if 'delay' in s['arrival']:
                arr_delay = s['arrival']['delay']
            else:
                arr_delay = None
        else:
            arr_delay = None
        if 'departure' in s:
            if 'delay' in s['departure']:
                dep_delay = s['departure']['delay']
            else:
                dep_delay = None
        else:
            dep_delay = None

        output = '{"time": %i, "trip_id": "%s", "stop_id": "%s", "arr_delay": %s, "dep_delay": %s}\n' % (timestamp, trip_id, stop_id, arr_delay, dep_delay)
        #print(output.replace("None", "null"))
        lines.append(output.replace("None", "null"))
    return lines


def check_folder(dir):
    """ check if folder exists / create folder"""
    if not os.path.exists(dir):
        os.makedirs(dir)


def produce_file():
    try:
        while True:
            print("about to request real-time feed")
            data = feed.real_time_request()
            timestamp = data['header']['timestamp']
            check_folder(filebase)
            filename = filebase + "/" + str(timestamp) + ".json"
            with open(filename, 'w') as dl:
                print("write to file", filename)
                for trip_update in data['entity']:
                    lines = input_normalizer(timestamp, trip_update)
                    for ln in lines:
                        dl.write(ln)
            print("wait 30 seconds")
            time.sleep(30)

    except HTTPError as e:  # most probably too many connections
        print("too many connections", e)
    except Exception as e:
        print("ex", e)
        traceback.print_exc()


def main():
    produce_file()


if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)