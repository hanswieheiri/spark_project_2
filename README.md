# Big Data Project (Advanced) #

Analyzing content of real-time API from SBB.

Data from [these](https://opentransportdata.swiss/de/dataset/go-realtime/resource/e264ed3b-5592-4d7d-aa0a-2fa19607567d) 
providers are in real-time. Currently **Verkehrsbetriebe Zürich** with id *849* is implemented.

## Prerequisites
* Python 3.6.x
* Get an API key for GTFS-RT and copy it to `api_key.yml`
  * To untrack: `git update-index --assume-unchanged api_key.yml`
  * To track: `git update-index --no-assume-unchanged api_key.yml`

if Python 2.7 is the standard python on your machine define in `.bash_profile`:
```
export PYSPARK_PYTHON=/Users/XXXX/anaconda/envs/py36/bin/python
export PYSPARK_DRIVER_PYTHON=/Users/XXXX/anaconda/envs/py36/bin/ipython
```

### Libraries
```
conda install -c conda-forge pyspark
conda install jupyter
```

## Run
```
jupyter notebook
```

### Download data
run `functions.download_static_data()`

### Zeppelin
```
brew install apache-zeppelin
```

update `interpretors.json` in `libexec/conf` directory, eg:
```
"properties": {
        "zeppelin.python": "/Users/xxx/anaconda/envs/py36/bin/python",
```

start:
```
bin/zeppelin-daemon.sh start
```

to stop:
```
bin/zeppelin-daemon.sh stop
```

got to [http://localhost:8080]