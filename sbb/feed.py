import json
import os
import urllib.request
import zipfile

import yaml

from sbb import functions_download

# globals
request_rt_feed_num_files = 12
request_rt_feed_mock_ix = -1


def read_conf(file='config.yml'):
    with open(file, 'r') as yml_file:
        return yaml.load(yml_file)


def download_feed_url(static_feed_url):
    """
    Request the API via HTTP request (GTFS).
    https://opentransportdata.swiss/de/cookbook/gtfs-rt/#Authorisierung_und_Open_Services
    """
    filename = functions_download.download_file(static_feed_url)
    print("downloaded: ", filename)
    #print("check filename")
    # if filename == "permalink":
    #     print("rename to zip")
    #     os.rename(filename, filename+".zip")
    #
    if filename.endswith(".zip") or filename == "permalink":
        print("unzip the data")
        zip_ref = zipfile.ZipFile(filename, 'r')
        unzip_dir = 'data'
        if not os.path.exists(unzip_dir):
            os.makedirs(unzip_dir)
        zip_ref.extractall(unzip_dir)
        zip_ref.close()


def request_rt_feed(rt_feed_url, key_file='api_key.yml'):
    """
    Request the API via HTTP request (GTFS-RT).
    Consider that one can only two 2 request per minute.

    https://opentransportdata.swiss/de/cookbook/gtfs-rt/#Authorisierung_und_Open_Services
    E.g. dict of:
    {
        'trip_update': {
            'stop_time_update': [{
                'schedule_relationship': 'SCHEDULED',
                'departure': {
                    'delay': 0
                },
                'stop_id': '8501609:0:3',
                'stop_sequence': 0
            }],
            'trip': {
                'schedule_relationship': 'SCHEDULED',
                'start_time': '17:49:00',
                'trip_id': '28.TA.42-2-Y-j17-1.1.H',
                'route_id': 'TA+b5m9p',
                'start_date': '20170605'
            }
        },
        'id': '28.TA.42-2-Y-j17-1.1.H'
    }
    """

    # read api_key
    with open(key_file, 'r') as yml_file:
        key_file = yaml.load(yml_file)
    api_key = key_file['rt_api_key']

    # make a HTTP GET request
    req = urllib.request.Request(rt_feed_url)
    req.add_header('Authorization', api_key)
    with urllib.request.urlopen(req) as r:
        body = json.loads(r.read().decode('utf-8'), encoding='utf-8')
        return body


def request_rt_feed_mock(rt_feed_url, key_file='api_key.yml'):
    global request_rt_feed_mock_ix
    request_rt_feed_mock_ix += 1

    with open('real-time-mock/response-' + str(request_rt_feed_mock_ix % request_rt_feed_num_files) + ".json") as r:
        body = json.loads(r.read(), encoding='utf-8')
        return body


def get_dummy_delay():
    return """{
                  'trip_update': {
            'stop_time_update': [{
                'schedule_relationship': 'SCHEDULED',
                'departure': {
                    'delay': 0
                },
                'stop_id': '8501609:0:3',
                'stop_sequence': 0
            }],
            'trip': {
                'schedule_relationship': 'SCHEDULED',
                'start_time': '17:49:00',
                'trip_id': '28.TA.42-2-Y-j17-1.1.H',
                'route_id': 'TA+b5m9p',
                'start_date': '20170605'
            }
        },
        'id': '28.TA.42-2-Y-j17-1.1.H'
        }"""


def download_static_data():
    cfg = read_conf()
    # request_rt_feed(cfg['rt_feed_url'])
    download_feed_url(cfg['static_feed_url'])


def real_time_request():
    cfg = read_conf()
    # return request_rt_feed_mock(cfg['rt_feed_url'])
    return request_rt_feed(cfg['rt_feed_url'])


if __name__ == "__main__":
    download_static_data()
