params = {'sep': ',', 'inferSchema': True, 'header': True}


def read_csv_agency(spark):
    return spark.read.csv("data/agency.txt", **params)


def read_csv_calendar(spark):
    return spark.read.csv("data/calendar.txt", **params)


def read_csv_calendar_dates(spark):
    return spark.read.csv("data/calendar_dates.txt", **params)


def read_csv_feed_info(spark):
    return spark.read.csv("data/feed_info.txt", **params)


def read_csv_routes(spark):
    return spark.read.csv("data/routes.txt", **params)


def read_csv_stop_times(spark):
    return spark.read.csv("data/stop_times.txt", **params)


def read_csv_stops(spark):
    return spark.read.csv("data/stops.txt", **params)


def read_csv_trips(spark):
    return spark.read.csv("data/trips.txt", **params)
